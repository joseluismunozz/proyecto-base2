import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GenericService {

  protected myAppUrl = environment.base_url;

  constructor(private httpClient: HttpClient) {}

  public getGeneric(controller: string, options: any): Observable<any>{
    console.log("Ruta Get", this.myAppUrl + controller + options);
    
    return this.httpClient.get<any>(this.myAppUrl + controller + options);
  }

  public postGeneric(controller: string, options: any, body:any): Observable<any>{
    console.log("Ruta post", this.myAppUrl + controller + options + body);

    let headers:any = new HttpHeaders();
    
    headers = headers.append('Content-Type', 'multipart/form-data')
    
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'multipart/form-data;'
       
      })
    };
    return this.httpClient.post<any>(this.myAppUrl + controller + options, body, headers) ;
  }


}
