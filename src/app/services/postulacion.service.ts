import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';

@Injectable({
  providedIn: 'root'
})
export class PostulacionService {
  _controllerPostulacion :string;

  constructor(private genericServices : GenericService) { 
    this._controllerPostulacion = "/postulacion"

    
  }

  savePostulacion(body : FormData){
    return this.genericServices.postGeneric( this._controllerPostulacion  ,'/save' , body);
  }
}
