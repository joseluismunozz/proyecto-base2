import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Event, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';
import { UsuariosService } from 'src/app/admin/services/usuarios.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.css']
})
export class SideMenuComponent implements OnInit {

  public pageTitle?: any;
  currentRoute?: string;

  mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;

  title = 'CEC';

  names?: string = '';
  surNames?: string = '';
  role?: string = '';

  public totalSolicitudes = 0;
  hidden:boolean = false;

  constructor(changeDetectorRef: ChangeDetectorRef, 
              media: MediaMatcher, 
              public authService : AuthService ,
              public userService : UsuariosService,
               private route: ActivatedRoute,
                private router: Router) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);

    

  }

  ngOnInit(): void {
    this.getUsers();
    this.authService.handdleRole();

    this.names = localStorage.getItem('nombres')! ;
    this.surNames = localStorage.getItem('apellidos')! ;
    this.role = localStorage.getItem('rol')! ;

    this.getRouteName();
    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationStart) {
          // Show loading indicator
          console.log('Route change detected');

          

      }

      if (event instanceof NavigationEnd) {
          // Hide loading indicator
          this.currentRoute = event.url;          
            console.log(event);
            
            this.getRouteName();
      }

      if (event instanceof NavigationError) {
          // Hide loading indicator

          // Present error to user
          console.log(event.error);
      }
  });
   

  }

  

  handleLogout(){
    this.authService.logout();
  }

  profile(){
    alert('perfil')
  }

  getRouteName(){
    this.route.url.subscribe(()=>{
      console.log(this.route?.snapshot?.firstChild?.data['title'])
      this.pageTitle =  this.route?.snapshot?.firstChild?.data['title']
    });
  }

  getUsers(){
    this.userService.getUsers().subscribe({
      next:(res : any)=>{

        if(res.length == 0){
          this.hidden = true;
        }
        
        this.totalSolicitudes = res.length;

        
        
      }
    })
  }

}
