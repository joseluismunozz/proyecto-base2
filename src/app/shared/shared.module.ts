import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedRoutingModule } from './shared-routing.module';
import { SideMenuComponent } from './side-menu/side-menu.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';

import { ReactiveFormsModule } from '@angular/forms';


import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatDialogModule} from '@angular/material/dialog';
import {MatTableModule} from '@angular/material/table';
import {MatBadgeModule} from '@angular/material/badge';
import {MatStepperModule} from '@angular/material/stepper';

import {AccordionModule} from 'primeng/accordion'; 

import {BreadcrumbModule} from 'xng-breadcrumb';

import { NgxMatFileInputModule } from '@angular-material-components/file-input';

import { SplitButtonModule } from 'primeng/splitbutton';

import {TableModule} from 'primeng/table';


@NgModule({
  declarations: [
    SideMenuComponent,
    WelcomeComponent,
    BreadcrumbComponent
  ],
  imports: [
    CommonModule,
    SharedRoutingModule,
    BreadcrumbModule,
    
    MatCardModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule,
    MatExpansionModule,
    ReactiveFormsModule,
    MatInputModule,
    MatSelectModule,
    MatCheckboxModule,
    MatSnackBarModule,
    MatDialogModule,
    MatTableModule,
    MatBadgeModule,
    MatStepperModule,
    NgxMatFileInputModule,
    AccordionModule,
    SplitButtonModule,
    TableModule
    
  ],
  exports : [
    SideMenuComponent,
    WelcomeComponent,
    BreadcrumbComponent,
    MatCardModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule,
    MatExpansionModule,
    MatInputModule,
    MatSelectModule,
    MatCheckboxModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatDialogModule,
    MatTableModule,
    MatBadgeModule,
    MatStepperModule,
    NgxMatFileInputModule,
    AccordionModule,
    SplitButtonModule,
    TableModule
  ]
})
export class SharedModule { }
