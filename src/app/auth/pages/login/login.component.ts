import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

import {MatSnackBar} from '@angular/material/snack-bar';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  hide = true;
  hideConfirm = true;

  loginForm: FormGroup;

  

  emailRegex =
    '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';

  constructor(private readonly fb: FormBuilder, 
    private readonly router: Router,
     private authService : AuthService,
     private _snackBar: MatSnackBar) {
    this.loginForm = this.fb.group({

      email: ['', [Validators.required, Validators.pattern(this.emailRegex)]],

      password: ['', [Validators.required]],



    })
  }

  ngOnInit(): void {
  }

  onSubmit():void {
    

    this.authService.login(this.loginForm.value).subscribe({
      
      next:(res)=>{
        console.log("RESPUESTA DE SERVICIO")
        console.log(res.role);
        if(res.role == 'PRESIDENTE'){
          this.router.navigate(['admin/'])
        }else if(res.role == 'INVESTIGADOR'){
          this.router.navigate(['investigador/'])
        }
      },

      error:(res)=>{
        console.log("ERROR" , res.status)
        
        this._snackBar.open("Datos incorrectos! 😱", "OK");
      }

      
    })

    
  }

  goToRegister(): void {
    this.router.navigate(['auth/register'])
  }

  goToRecoverEmail(): void {
    this.router.navigate(['auth/forgot'])
  }

}
