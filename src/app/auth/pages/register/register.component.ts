import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';
import { DialogRegisterComponent } from '../../components/dialog-register/dialog-register.component';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  hide = true;
  hideConfirm = true

  registerForm: FormGroup;
  emailRegex =
    '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';

  foods = [
    { value: 'RUT', viewValue: 'RUT' },
    { value: 'Pasaporte', viewValue: 'Pasaporte' },
  ];

  /*
  passwordMatchingValidatior: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
    const password = control.get('password');
    const confirmPassword = control.get('password_confirm');

    return password?.value === confirmPassword?.value ? null : { passwordMismatch: true };
  }; */



  constructor(private readonly fb: FormBuilder, private readonly router: Router, private authService: AuthService, private _snackBar: MatSnackBar, public dialog: MatDialog) {
    this.registerForm = this.fb.group({
      names: ['', [Validators.required, Validators.minLength(4)]],
      surnames: ['', [Validators.required, Validators.minLength(4)]],
      document_type: ['', [Validators.required]],
      document: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.pattern(this.emailRegex)]],
      phone: ['', [Validators.required]],
      password: ['', [Validators.required]],
      password_confirm: ['', [Validators.required ] ],
      checkTerms: ['', [Validators.requiredTrue]],


    }, 
  //  { validators: this.passwordMatchingValidatior }
    )

    // tslint:disable-next-line:no-non-null-assertion
    this.registerForm!
      .get('password_confirm')!
      // tslint:disable-next-line:no-non-null-assertion
      .setValidators([Validators.required, CustomValidators.equals(this.registerForm.get('password')!)]);
  }

  ngOnInit(): void {
  }

  onTest(): void {
    this.openDialog();
  }

  onSubmit(): void {
   // console.log('Form ->', this.registerForm.value)
    this.authService.register(this.registerForm.value).subscribe({
      next:(res)=>{
        console.log('res en componente',res)
        this.openDialog();
        this.router.navigate(['auth/login'])
      },

      error:(res)=>{
        console.log("ERROR" , res.error.message)
        
        this._snackBar.open( res.error.message , "OK");
      }
    })
    //this.contactForm.reset();
  }

  openDialog() {
    this.dialog.open(DialogRegisterComponent);
  }

  goToLogin(): void {
    this.router.navigate(['auth/login'])
  }

}


function equalsValidator(otherControl: AbstractControl): ValidatorFn {

  return (control: AbstractControl): { [key: string]: any } | null => {
    const value: string = control.value;
    const otherValue: any = otherControl.value;
    return otherValue === value ? null : {notEquals: {value, otherValue}};
  };
} 


export const CustomValidators = {
  equals: equalsValidator
};


