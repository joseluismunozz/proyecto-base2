import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InvestigadorRoutingModule } from './investigador-routing.module';
import { CrearSolicitudComponent } from './components/crear-solicitud/crear-solicitud.component';
import { SharedModule } from '../shared/shared.module';




@NgModule({
  declarations: [
    CrearSolicitudComponent
  ],
  imports: [
    CommonModule,
    InvestigadorRoutingModule,
    SharedModule
    
  ]
})
export class InvestigadorModule { }
