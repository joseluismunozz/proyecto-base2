import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PostulacionService } from 'src/app/services/postulacion.service';

@Component({
  selector: 'app-crear-solicitud',
  templateUrl: './crear-solicitud.component.html',
  styleUrls: ['./crear-solicitud.component.css']
})
export class CrearSolicitudComponent implements OnInit {

//  public demoForm: FormGroup;


  firstFormGroup : any = this._formBuilder.group({
    id: [localStorage.getItem('id')!, Validators.required],
    name: [localStorage.getItem('nombres')!, Validators.required],
   // title: [localStorage.getItem('nombres')!, Validators.required],
  });

  secondFormGroup = this._formBuilder.group({
    secondCtrl: ['', Validators.required],
  });

  demoForm : any = this._formBuilder.group({
    //text_input: ['', Validators.required],
    photos: this._formBuilder.array([])
 });

  



  isLinear = true;

  constructor(private _formBuilder: FormBuilder, 
    private _snackBar: MatSnackBar,
      public postulacionService : PostulacionService ) { 

  //   this.demoForm = this._formBuilder.group({
    //  text_input: ['', Validators.required],
     // photos: this._formBuilder.array([])
  // });

  }

  ngOnInit(): void {
  }

  onFinish(){

    let formData = new FormData();
    //console.log('Title form ->', this.firstFormGroup.value)
    console.log('IMAGES FORMs ->', this.demoForm)
  
    
    this.demoForm.value.photos?.map((file : any  ) =>{
      formData.append('documents', file.file )
      return file.file
    })
    
    formData.append('userId', this.firstFormGroup.value.id )
    formData.append('title', this.firstFormGroup.value.name )

   
  console.log('formDataBody ->', formData)
    

    this.postulacionService.savePostulacion(formData).subscribe({
      next:(res)=>{
        console.log('res' ,res);
       //this.dialogRef.close();
       //window.location.reload();
       this._snackBar.open("Solicitud enviada ✅", "OK" , {duration: 2 * 1000} );
       
      },
      error: (res)=>{
        console.log('ERROR ->', res)
      }
    }) 

    //prearar array de files

    //preparar body


    //enviar datos por post
  }

  // We will create multiple form controls inside defined form controls photos.
  createItem(data : any): FormGroup {
    return this._formBuilder.group(data);
  }

   //Help to get all photos controls as form array.
   get photos(): any {
     
    return this.demoForm.get('photos') as any;
  };



  detectFiles(event : any) {
    //alert('Nuevo archivo detectado')
    let files = event.target.files;
    if (files) {
      for (let file of files) {
        let reader = new FileReader();
        reader.onload = (e: any) => {
          
        //  console.log("e.target.result", e.target.result);
            this.photos.push(this.createItem({
                file,
                //url: e.target.result  //Base64 string for preview image
            }));

            console.log('nuevo photos',  this.photos.value)
        }
        reader.readAsDataURL(file);
      }
    }
  }

  removePhoto(i : any){
		this.photos.removeAt(i);
	}




}
