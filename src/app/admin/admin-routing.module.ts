import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SideMenuComponent } from '../shared/side-menu/side-menu.component';
import { WelcomeComponent } from '../shared/welcome/welcome.component';
import { SolicitudesComponent } from './components/solicitudes/solicitudes.component';
import { CrudTestComponent } from './pages/crud-test/crud-test.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '', component: SideMenuComponent, children: [
        
          //{ path: 'crud', component: CrudTestComponent,  data: { breadcrumb: 'Solicitud de registros' , title : 'Solicitud de registros'} },
          { path: 'solicitudes', component: SolicitudesComponent,  data: { breadcrumb: 'Solicitud de registros' , title : 'Solicitud de registros'} },
          { path: 'welcome', component: WelcomeComponent, data: {title : 'Inicio', breadcrumb: {skip : true} } },
          { path: '',  redirectTo: 'welcome', pathMatch: 'full', data: { breadcrumb: 'Inicio' , title : 'Inicio' }  }, 
        ]
      },

      { path: '**', redirectTo: 'main', pathMatch: 'full' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
