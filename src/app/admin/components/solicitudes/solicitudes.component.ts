import { Component, OnInit } from '@angular/core';
import { UsuariosService } from '../../services/usuarios.service';
import { PrimeNGConfig } from 'primeng/api';

import {MatDialog} from '@angular/material/dialog';
import { DialogAproveUserComponent } from '../dialog-aprove-user/dialog-aprove-user.component';



@Component({
  selector: 'app-solicitudes',
  templateUrl: './solicitudes.component.html',
  styleUrls: ['./solicitudes.component.css']
})
export class SolicitudesComponent implements OnInit {

  displayedColumns!: string[];
  dataSource: any;


  constructor(private userService: UsuariosService, public dialog: MatDialog,private primengConfig: PrimeNGConfig) { }

  ngOnInit(): void {
    this.primengConfig.ripple = true;

    this.getSolicitudes();
   
  }

  getSolicitudes(){
    this.userService.getUsers().subscribe(res =>{
      console.log("res Usser", res);
      this.displayedColumns = ['name', 'surnames', 'email', 'actions'];
      this.dataSource = res;
    });
  }
  aproveUser(user : any){
    console.log(user)
    this.dialog.open(DialogAproveUserComponent, {
      width: '350px',
      data: user,
    });
  }

}
